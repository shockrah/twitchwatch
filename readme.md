# Twitch Keeper

Monitoring/moderation tool I use for [my own stream](https://twitch.tv/shockrahwow).

> What does it do?

It let's me easily interface with the twitch bot which user commands during a livestream.

Written in Python3 with some of the newer `async` features offered by 3.6+.
