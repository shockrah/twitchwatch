# Smol module to make text look pretty
import random
# Prefix = \u001b[
ansi_codes = {
    'black':'30m',
    'red': '31m',
    'green': '32m',
    'yellow': '33m',
    'blue': '34m',
    'magenta': '35m',
    'cyan': '36m',
    'white': '37m',
}
reset = '0m'
prefix = '\u001b['
def random_color(text):
    color = ansi_codes[random.choice(list(ansi_codes.keys()))]
    return f'{prefix}{color}{text}{reset}'
