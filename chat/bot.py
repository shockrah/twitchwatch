import sys
import select
import socket
import colors
import cfg

class Bot:
    def __init__(self, name, channel, password):
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.irc.settimeout(10)
        self.timeout = 5 # in seconds

        # Used for joining the channel
        self.nick = name
        self.chan = channel
        self.password = password
        self.host = 'irc.twitch.tv'
        self.port = 6667
        self.connect()

    def send(self, msg):
        self.irc.send(f'PRIVMSG {self.CHAN} {msg}'.encode('utf-8'))

    def connect(self):
        self.irc.connect((self.host, self.port))
        # authentication - this part has to be a little special for server reasons
        self.irc.send(f'PASS oauth:{self.password}'.encode('utf-8'))
        self.irc.send(f'NICK {self.nick}'.encode('utf-8'))
        self.irc.send(f'JOIN #{self.chan}'.encode('utf-8'))

    def terminate(self):
        print('Closing socket')
        self.irc.close()

    def get_buffer(self):
        ready = select.select([self.irc], [self.irc],[self.irc], self.timeout)
        if ready[0]:
            ret = self.irc.recv(2048)
            return str(ret)[2:-1]

        print(ready[0])

    def tokenize(self, buf: str):
        # :USERNAME!__garbage__:messasge\r\n
        ret = {'name':None, 'msg':None}

        # Pulling the name first
        try:
            ret['name'] = buf[:buf.find('!')]
            # Pull out the message itself now
            # NOTE: the -4 is to avoid pulling out the \r\n
            # NOTE: the +1 is to avoid reading the first :(colon)
            tmp = buf[1:]
            ret['msg'] = tmp[tmp.find(':')+1:-4]
        except AttributeError as e:
            print('ERROR', e, file=sys.stderr)
        finally:
            return ret


    def listen(self):
        failure = False
        while failure is False:
            tokens = self.tokenize(self.get_buffer())
            name = tokens['name']
            msg = tokens['msg']

            if msg == "PING":
                self.send('PONG')
            print(f'{colors.random_color(name)}: {msg}')
            failure = msg is None and name is None
        if failure:
            self.terminate()


if __name__ == "__main__":
    bot = Bot(name='shockrahbot', channel='shockrah',password=cfg.password)
    try:
        bot.listen()
    except Exception:
        bot.terminate()
    except KeyboardInterrupt:
        bot.terminate()
