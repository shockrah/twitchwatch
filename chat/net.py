'''
This module deals with the lower level socket connections that we are required
to make in order to establish connection to twitch's servers
'''
import socket
import cfg

# Generic constants for connecting to twitch
HOST	= 'irc.chat.twitch.tv'
PORT	= 6667	# default port for all twitch chat channels
CHAN	= None
RATE	= "20/30"

SOCKET 	= socket.socket()

# Creates a socket for the chatbot
def irc_connect():
    '''
    Performs authentication steps to join a twitch channel
    Which by the way is really weird and not like normal irc servers
    '''
    try:
        SOCKET.connect((HOST, PORT))

        # Joining stuff
        print('sending auth data')
        send(f'PASS oauth:{cfg.PASS}')
        send(f'NICK {cfg.NAME}')
        send(f'JOIN #{CHAN}')

    except Exception as e:
        print(f'{e} caught!\n Socket creation failed port might be taken')
        exit(0)

# only used for the ping/pong thing
def send(msg):
    '''
    Use for sending messages to twitch however we only use this for 'ping's
    '''
    SOCKET.send(f'{msg}\r\n'.encode('utf-8'))

# half the reason why this is here is because
def connect() -> None:
	print('Connecting to irc channel')
	irc_connect()

# closing connection cleanly so that we don't fuck up our thing
def clean():
	print('Closing socket')
	SOCKET.close()
	exit(0)

def buffer() -> str:
	"""
	Returns the string version of our buffer
	"""
	ret = SOCKET.recv(1024)
	return str(ret)[2:-1]
