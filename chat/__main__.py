'''
Usage python chat -c <channel_name>
Used to watch chat basically since I don't want to bother having 999 windows open
Also because it's a minor flex on people if I have a terminal open just for them
'''
#!/usr/bin/python3
import argparse
from bot import Bot
import cfg

if __name__ == '__main__':
    # general description given no args
    parser = argparse.ArgumentParser(description='Monitor twitch chat')

    parser.add_argument('-c', '--channel',help='Watches CHANNEL\'s chat', required=True)
    channel  = cfg.channel
    nickname = cfg.nickname
    password = cfg.password

    args = vars(parser.parse_args())
    bot = None
    if args['channel']:
        channel = args['channel']
        bot = Bot(name=nickname, channel=channel, password=password)
    else:
        bot = Bot(name=cfg.nickname, channel=cfg.channel, password=cfg.password)

    try:
        bot.listen()

    except KeyboardInterrupt:
        bot.terminate()
